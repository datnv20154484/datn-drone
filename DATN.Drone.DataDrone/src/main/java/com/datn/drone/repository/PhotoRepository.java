package com.datn.drone.repository;


import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.datn.drone.model.Photo;

public interface PhotoRepository extends MongoRepository<Photo, String> {
	
	List<Photo> findByidpole(String idpole);
	List<Photo> findBydateImport(String dateimport);
	List<Photo> findBydateCreate(String datecreate);
	List<Photo> findByiddrone(String iddrone);
}
