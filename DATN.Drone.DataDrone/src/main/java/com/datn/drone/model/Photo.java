package com.datn.drone.model;

import org.bson.types.Binary;
import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "Photos")
public class Photo {

	@Id
	public ObjectId _id;

	private String title;
	private String dateCreate;
	private String dateImport;
	private String description;
	private String idpole;
	private String iduser;
	private String iddrone;
	private boolean crop;
	private Binary image;

	public String get_id() {
		return _id.toHexString();
	}

	public void set_id(ObjectId _id) {
		this._id = _id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public Binary getImage() {
		return image;
	}

	public void setImage(Binary image) {
		this.image = image;
	}

	public String getDateCreate() {
		return dateCreate;
	}

	public void setDateCreate(String dateCreate) {
		this.dateCreate = dateCreate;
	}

	public String getDateImport() {
		return dateImport;
	}

	public void setDateImport(String dateImport) {
		this.dateImport = dateImport;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getIdpole() {
		return idpole;
	}

	public void setIdpole(String idpole) {
		this.idpole = idpole;
	}

	public boolean isCrop() {
		return crop;
	}
	
	public void setCrop(boolean crop) {
		this.crop = crop;
	}

	public String getIduser() {
		return iduser;
	}

	public void setIduser(String iduser) {
		this.iduser = iduser;
	}

	public String getIddrone() {
		return iddrone;
	}

	public void setIddrone(String iddrone) {
		this.iddrone = iddrone;
	}

	

	@Override
	public String toString() {
		return "Photo [_id=" + _id + ", title=" + title + ", dateCreate=" + dateCreate + ", dateImport=" + dateImport
				+ ", description=" + description + ", idpole=" + idpole + ", iduser=" + iduser + ", iddrone=" + iddrone
				+ ", crop=" + crop + "]";
	}

	public Photo(String title, String dateCreate, String dateImport, String description, String idpole, String iduser,
			String iddrone, boolean crop) {
		super();
		this.title = title;
		this.dateCreate = dateCreate;
		this.dateImport = dateImport;
		this.description = description;
		this.idpole = idpole;
		this.iduser = iduser;
		this.iddrone = iddrone;
		this.crop = crop;
	}

	public Photo() {
		// TODO Auto-generated constructor stub
	}

	
}
