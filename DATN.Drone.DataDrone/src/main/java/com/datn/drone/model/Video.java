package com.datn.drone.model;

import java.io.InputStream;


public class Video {
	
    private String title;
    private String dateCreate;
	private String dateImport;
	private String description;
	private String idpole;
	private String iduser;
	private String iddrone;
    private InputStream stream;
    
    public Video() {
        super();
    }

    
	public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public InputStream getStream() {
        return stream;
    }

    public void setStream(InputStream stream) {
        this.stream = stream;
    }

	public String getDateCreate() {
		return dateCreate;
	}

	public void setDateCreate(String dateCreate) {
		this.dateCreate = dateCreate;
	}

	public String getDateImport() {
		return dateImport;
	}

	public void setDateImport(String dateImport) {
		this.dateImport = dateImport;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getIdpole() {
		return idpole;
	}

	public void setIdpole(String idpole) {
		this.idpole = idpole;
	}

	public String getIduser() {
		return iduser;
	}

	public void setIduser(String iduser) {
		this.iduser = iduser;
	}

	public String getIddrone() {
		return iddrone;
	}

	public void setIddrone(String iddrone) {
		this.iddrone = iddrone;
	}


}
