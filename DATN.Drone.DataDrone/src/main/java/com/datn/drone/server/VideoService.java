package com.datn.drone.server;

import java.io.IOException;
import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.datn.drone.controller.VideoController;
import com.datn.drone.model.Video;

@RestController
@RequestMapping("/videos")
public class VideoService{

	 @Autowired
	    private VideoController videoService;

	    @GetMapping("/{id}")
	    public String getVideo(@PathVariable String id) throws IllegalStateException, IOException {
	        Video video = videoService.getVideo(id);
	        return video.getTitle();
	    }
	    @GetMapping("/allid")
	    public List<String> getall() throws IllegalStateException, IOException {
	        return videoService.getAllVideo();
	    }
	    
	    @GetMapping("/countall")
	    public int count() throws IllegalStateException, IOException {
	        return videoService.getAllVideo().size();
	    }


	    @GetMapping("/stream/{id}")
	    public void streamVideo(@PathVariable String id, HttpServletResponse response) throws IllegalStateException, IOException {
	        Video video = videoService.getVideo(id);
	        FileCopyUtils.copy(video.getStream(), response.getOutputStream());
	    }

	    @PostMapping("/add")
	    public String addVideo(@RequestParam("title") String title, @RequestParam("dateCreate") String dateCreate,
				@RequestParam("dateImport") String dateImport, @RequestParam("des") String des,@RequestParam("idpole") String idpole,
				@RequestParam("iduser") String iduser,@RequestParam("iddrone") String iddrone, @RequestParam("file") MultipartFile file, Model model) throws IOException {
	        String id = videoService.addVideo(title, dateCreate, dateImport, des,idpole,iduser,iddrone, file);
	        return id;
	    }
	    
	    @DeleteMapping("videos/{id}")
	    public void deleteVideo(@PathVariable String id) throws IllegalStateException, IOException {
	    	videoService.delete(id);
	    }
}
