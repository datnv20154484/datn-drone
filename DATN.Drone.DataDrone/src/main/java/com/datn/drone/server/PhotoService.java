package com.datn.drone.server;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.datn.drone.controller.PhotoController;
import com.datn.drone.model.Photo;

@RestController
@RequestMapping("/photos")
public class PhotoService {

	@Autowired
	private PhotoController photoService;

	@GetMapping("/{id}")
	public Photo getPhoto(@PathVariable String id) {
		Photo photo = photoService.getPhoto(id);
		return photo;
	}
	@GetMapping(
			  value = "/byte/{id}",
			  produces = MediaType.IMAGE_JPEG_VALUE
			)
	public @ResponseBody byte[] getPhotoBinary(@PathVariable String id) throws IOException {
		Photo photo = photoService.getPhoto(id);
		return photo.getImage().getData();
	}
	
	@GetMapping("/imagecompare")
	public double compare(@RequestParam("idimage1") String idimage1, @RequestParam("idimage2") String idimage2) {
		double photo = 0;
		try {
			photo = photoService.CompareImage(idimage1, idimage2);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return photo;
	}
	
	@GetMapping("/crop/{id}")
	public Photo cropPhoto(@PathVariable String id, Model model) throws IOException {
		Photo photo = photoService.CropImage(id);
		return photo;
	}

	@GetMapping("/listimagecheck")
	public List<String> getlistcheck(@RequestParam("idpole") String idpole, @RequestParam("date") String date,@RequestParam("crop") boolean crop) {
		List<String> photo = photoService.getimagecheckinci(idpole, date, crop);
		return photo;
	}
	
	@GetMapping("/allid")
	public List<String> getAllIDPhoto() {
		List<Photo> photo = photoService.getAllPhoto();
		List<String> lsid = new ArrayList<>();
		int sum = photo.size();
		for(int i = 0; i < sum; i++) {
			lsid.add(photo.get(i).get_id());
		}
		return lsid;
	}
	@GetMapping("/countall")
	public int count() {
		List<Photo> photo = photoService.getAllPhoto();
		return photo.size();
	}
	
	@GetMapping("/all")
	public List<Photo> getAllPhoto() {
		List<Photo> photo = photoService.getAllPhoto();
		
		
		
		return photo;
	}
	
	@PostMapping("/add")
	public String addPhoto(@RequestParam("title") String title, @RequestParam("dateCreate") String dateCreate,
			@RequestParam("dateImport") String dateImport, @RequestParam("des") String des,
			@RequestParam("idpole") String idpole, @RequestParam("iduser") String iduser, @RequestParam("iddrone") String iddrone,@RequestParam("crop") boolean crop, @RequestParam("image") MultipartFile image)
			throws IOException {
		String id = photoService.addPhoto(title, dateCreate, dateImport, des, idpole, iduser, iddrone, crop, image);
		return id;
	}

	@DeleteMapping("/{id}")
	public ResponseEntity<String> deletePhoto(@PathVariable String id) {
		photoService.deletePhoto(id);
		return new ResponseEntity<>("Photo delete successsfully", HttpStatus.OK);
	}
	

}
