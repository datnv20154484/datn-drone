package com.datn.drone.controller;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import javax.imageio.ImageIO;
import org.bson.BsonBinarySubType;
import org.bson.types.Binary;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import com.datn.drone.model.Photo;
import com.datn.drone.repository.PhotoRepository;


@Service
public class PhotoController {
	
	@Autowired
	private PhotoRepository photoRepo;

	public List<Photo> getAllPhoto() {
		return photoRepo.findAll();
	}

	public Photo getPhoto(String id) {
		return photoRepo.findById(id).get();
	}

	public String addPhoto(String title, String dateCreate, String dateImport, String des, String idpole, String iduser, String iddrone,boolean crop,
			MultipartFile file) throws IOException {
		Photo photo = new Photo(title, dateCreate, dateImport, des, idpole,iduser,iddrone, crop);
		photo.set_id(ObjectId.get());
		photo.setImage(new Binary(BsonBinarySubType.BINARY, file.getBytes()));
		photo = photoRepo.insert(photo);
		return photo.get_id();
	}

	public void deletePhoto(String id) {
		photoRepo.deleteById(id);
	}

	public Photo CropImage(String id) throws IOException {
		Photo p1 = photoRepo.findById(id).get();
		byte[] Im = p1.getImage().getData();
		ByteArrayInputStream bais = new ByteArrayInputStream(Im);
		BufferedImage bf = ImageIO.read(bais);
		BufferedImage crop = bf.getSubimage(300, 300, 200, 200);
		System.out.println(crop);
		File outputfile = new File("C:/Users/Dom/Desktop/anhdat123.jpg");
		ImageIO.write(crop, "jpg", outputfile);
		System.out.println("Image cropped successfully: " + outputfile.getPath());

		return p1;
	}
	
	public double CompareImage(String idimage1, String idimage2) throws IOException {
		double result = 0;
		
		Photo image1 = photoRepo.findById(idimage1).get();
		byte[] Im1 = image1.getImage().getData();
		ByteArrayInputStream bais1 = new ByteArrayInputStream(Im1);
		BufferedImage imgA = ImageIO.read(bais1);
		Photo image2 = photoRepo.findById(idimage2).get();
		byte[] Im2 = image2.getImage().getData();
		ByteArrayInputStream bais2 = new ByteArrayInputStream(Im2);
		BufferedImage imgB = ImageIO.read(bais2);
		int width1 = imgA.getWidth();
		int width2 = imgB.getWidth();
		int height1 = imgA.getHeight();
		int height2 = imgB.getHeight();

		if ((width1 != width2) || (height1 != height2))
			System.out.println("Error: Images dimensions" + " mismatch");
		else {
			long difference = 0;
			for (int y = 0; y < height1; y++) {
				for (int x = 0; x < width1; x++) {
					int rgbA = imgA.getRGB(x, y);
					int rgbB = imgB.getRGB(x, y);
					int redA = (rgbA >> 16) & 0xff;
					int greenA = (rgbA >> 8) & 0xff;
					int blueA = (rgbA) & 0xff;
					int redB = (rgbB >> 16) & 0xff;
					int greenB = (rgbB >> 8) & 0xff;
					int blueB = (rgbB) & 0xff;
					difference += Math.abs(redA - redB);
					difference += Math.abs(greenA - greenB);
					difference += Math.abs(blueA - blueB);
				}
			}

			// Total number of red pixels = width * height
			// Total number of blue pixels = width * height
			// Total number of green pixels = width * height
			// So total number of pixels = width * height * 3
			double total_pixels = width1 * height1 * 3;

			// Normalizing the value of different pixels
			// for accuracy(average pixels per color
			// component)
			double avg_different_pixels = difference / total_pixels;

			// There are 255 values of pixels in total
			result = (avg_different_pixels / 255) * 100;

			System.out.println("Difference Percentage-->" + result);
		}
		return result;
	}
	
	public List<String> getimagecheckinci(String idpole, String date, boolean crop) {
		List<Photo> list = photoRepo.findBydateCreate(date);
		List<String> result = new ArrayList<>();
		for(Photo p : list) {
			if(p.getIdpole().equals(idpole) && crop == p.isCrop()) {
				result.add(p.get_id());
			}
			
		}
		return result;
		
	}
}
