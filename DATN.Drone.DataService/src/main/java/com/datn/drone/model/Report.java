package com.datn.drone.model;

import java.util.Date;

import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "Report")
public class Report {

	@Id
	public ObjectId _id;
	private String Report_Name;
	private String Report_Des;
	private String Report_Type;
	private Date Report_DateCreate;
	private Date Report_DateUpdate;
	private String Report_EmpID;
	private String Report_link;
	
	public String get_id() {
		return _id.toHexString();
	}

	public void set_id(ObjectId _id) {
		this._id = _id;
	}

	public String getReport_Name() {
		return Report_Name;
	}

	public void setReport_Name(String report_Name) {
		Report_Name = report_Name;
	}

	public String getReport_Des() {
		return Report_Des;
	}

	public void setReport_Des(String report_Des) {
		Report_Des = report_Des;
	}

	public String getReport_Type() {
		return Report_Type;
	}

	public void setReport_Type(String report_Type) {
		Report_Type = report_Type;
	}

	public Date getReport_DateCreate() {
		return Report_DateCreate;
	}

	public void setReport_DateCreate(Date report_DateCreate) {
		Report_DateCreate = report_DateCreate;
	}

	public Date getReport_DateUpdate() {
		return Report_DateUpdate;
	}

	public void setReport_DateUpdate(Date report_DateUpdate) {
		Report_DateUpdate = report_DateUpdate;
	}

	public String getReport_EmpID() {
		return Report_EmpID;
	}

	public void setReport_EmpID(String report_EmpID) {
		Report_EmpID = report_EmpID;
	}

	public String getReport_link() {
		return Report_link;
	}

	public void setReport_link(String report_link) {
		Report_link = report_link;
	}

}
