package com.datn.drone.model;

import java.util.Date;

import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "Drone")
public class Drone {
	@Id
	public ObjectId _id;
	private String name;
	private String des;
	private String idman;
	private boolean online;
	private int battery;
	private Date dateuse;
	private String status;
	private Date maintenancetime;

	public String get_id() {
		return _id.toHexString();
	}

	public void set_id(ObjectId _id) {
		this._id = _id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDes() {
		return des;
	}

	public void setDes(String des) {
		this.des = des;
	}

	public String getIdman() {
		return idman;
	}

	public void setIdman(String idman) {
		this.idman = idman;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Date getMaintenancetime() {
		return maintenancetime;
	}

	public void setMaintenancetime(Date maintenancetime) {
		this.maintenancetime = maintenancetime;
	}

	public boolean isOnline() {
		return online;
	}

	public void setOnline(boolean online) {
		this.online = online;
	}

	public int getBattery() {
		return battery;
	}

	public void setBattery(int battery) {
		this.battery = battery;
	}

	public Date getDateuse() {
		return dateuse;
	}

	public void setDateuse(Date dateuse) {
		this.dateuse = dateuse;
	}

	
}
