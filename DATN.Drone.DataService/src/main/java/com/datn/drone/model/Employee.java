package com.datn.drone.model;


import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "Employee")
public class Employee {
	@Id
	public ObjectId _id;
	
	public String username;
	public String password;
	public String role;
	public String name;
	public String birth;
	public String sex;
	public String nationid;
	public String address;
	public String mail;
	public String phone;

	// Constructors
	public Employee() {
	}


// ObjectId needs to be converted to string
	public String get_id() {
		return _id.toHexString();
	}

	public void set_id(ObjectId _id) {
		this._id = _id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}


	public String getSex() {
		return sex;
	}

	public void setSex(String sex) {
		this.sex = sex;
	}

	public String getNationid() {
		return nationid;
	}

	public void setNationid(String nationid) {
		this.nationid = nationid;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getMail() {
		return mail;
	}

	public void setMail(String mail) {
		this.mail = mail;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}


	public String getUsername() {
		return username;
	}


	public void setUsername(String username) {
		this.username = username;
	}


	public String getPassword() {
		return password;
	}


	public void setPassword(String password) {
		this.password = password;
	}


	public String getRole() {
		return role;
	}


	public void setRole(String role) {
		this.role = role;
	}


	public String getBirth() {
		return birth;
	}


	public void setBirth(String birth) {
		this.birth = birth;
	}


	public Employee(ObjectId _id, String username, String password, String role, String name, String birth, String sex,
			String nationid, String address, String mail, String phone) {
		super();
		this._id = _id;
		this.username = username;
		this.password = password;
		this.role = role;
		this.name = name;
		this.birth = birth;
		this.sex = sex;
		this.nationid = nationid;
		this.address = address;
		this.mail = mail;
		this.phone = phone;
	}

}