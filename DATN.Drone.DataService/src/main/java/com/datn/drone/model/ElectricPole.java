package com.datn.drone.model;

import java.util.Date;

import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "ElecPole")
public class ElectricPole {
	@Id
	public ObjectId _id;
	private String Pole_Name;
	private Double Pole_Latitude;
	private Double Pole_Longitude;
	private String Description;
	private Date BuildTime;
	private Date EP_MaintenanceTime;

	public String get_id() {
		return _id.toHexString();
	}

	public void set_id(ObjectId _id) {
		this._id = _id;
	}

	public String getPole_Name() {
		return Pole_Name;
	}

	public void setPole_Name(String pole_Name) {
		Pole_Name = pole_Name;
	}

	public String getDescription() {
		return Description;
	}

	public void setDescription(String description) {
		Description = description;
	}

	public Double getPole_Latitude() {
		return Pole_Latitude;
	}

	public void setPole_Latitude(Double pole_Latitude) {
		Pole_Latitude = pole_Latitude;
	}

	public Double getPole_Longitude() {
		return Pole_Longitude;
	}

	public void setPole_Longitude(Double pole_Longitude) {
		Pole_Longitude = pole_Longitude;
	}

	public Date getBuildTime() {
		return BuildTime;
	}

	public void setBuildTime(Date buildTime) {
		BuildTime = buildTime;
	}

	public Date getEP_MaintenanceTime() {
		return EP_MaintenanceTime;
	}

	public void setEP_MaintenanceTime(Date eP_MaintenanceTime) {
		EP_MaintenanceTime = eP_MaintenanceTime;
	}

}
