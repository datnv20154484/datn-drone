package com.datn.drone.server;

import java.util.List;
import javax.validation.Valid;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import com.datn.drone.controller.EmployeeController;
import com.datn.drone.model.Employee;

@RestController
@RequestMapping("/employees")
public class EmployeeServer {
	@Autowired
	private EmployeeController employeeController;

	@RequestMapping(value = "/", method = RequestMethod.GET)
	public List<Employee> getAllemployees() {
		return employeeController.getAllemployees();
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public Employee getEmpById(@PathVariable("id") ObjectId id) {
		return employeeController.getEmpById(id);
	}

	@RequestMapping(value = "/username/{username}", method = RequestMethod.GET)
	public Employee getEmpByusername(@PathVariable("username") String username) {
		return employeeController.getEmpByusername(username);
	}
	
	@RequestMapping(value = "/nationid/{nationid}", method = RequestMethod.GET)
	public Employee getEmpBynationid(@PathVariable("nationid") String nationid) {
		return employeeController.getEmpBynationid(nationid);
	}
	@RequestMapping(value = "/mail/{mail}", method = RequestMethod.GET)
	public Employee getEmpBymail(@PathVariable("mail") String mail) {
		return employeeController.getEmpBymail(mail);
	}

	@RequestMapping(value = "/phone/{phone}", method = RequestMethod.GET)
	public Employee getEmpByphone(@PathVariable("phone") String phone) {
		return employeeController.getEmpByphone(phone);
	}
	
	@RequestMapping(value = "/role/{role}", method = RequestMethod.GET)
	public List<Employee> getEmpByrole(@PathVariable("role") String role) {
		return employeeController.getEmpByrole(role);
	}
	
	@RequestMapping(value = "/name/{name}", method = RequestMethod.GET)
	public List<Employee> getEmpByname(@PathVariable("name") String name) {
		return employeeController.getEmpByname(name);
	}
	
	@RequestMapping(value = "/birth/{birth}", method = RequestMethod.GET)
	public List<Employee> getEmpBybirth(@PathVariable("birth") String birth) {
		return employeeController.getEmpBybirth(birth);
	}
	
	@RequestMapping(value = "/sex/{sex}", method = RequestMethod.GET)
	public List<Employee> getEmpBysex(@PathVariable("sex") String sex) {
		return employeeController.getEmpBysex(sex);
	}
	
	@RequestMapping(value = "/address/{address}", method = RequestMethod.GET)
	public List<Employee> getEmpByaddress(@PathVariable("address") String address) {
		return employeeController.getEmpByaddress(address);
	}
	
	@RequestMapping(value = "/{id}", method = RequestMethod.PUT)
	public ResponseEntity<String> modifyEmpById(@PathVariable("id") ObjectId id, @Valid @RequestBody Employee employees) {
		employeeController.modifyEmpById(id, employees);
		return new ResponseEntity<>("Sửa thông tin nhân viên thành công", HttpStatus.OK);
	}
	@RequestMapping(value = "/", method = RequestMethod.POST)
	public boolean createEmp(@Valid @RequestBody Employee employees) {
		return employeeController.createEmp(employees);
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<String> deleteEmp(@PathVariable ObjectId id) {
		employeeController.deleteEmp(id);
		return new ResponseEntity<>("Xóa thông tin nhân viên thành công", HttpStatus.OK);
	}
	
	@RequestMapping(value = "/login/", method = RequestMethod.GET)
	public Employee getEmpByrole(@RequestParam String username, @RequestParam String password) {
		return employeeController.login(username, password);
	}
	
	@RequestMapping(value = "/send/", method = RequestMethod.GET)
	public  ResponseEntity<String> sendforgetmail(@RequestParam String phone) {
		employeeController.sendforgetmail(phone);
		return new ResponseEntity<>("Gửi Email phục hồi thành công", HttpStatus.OK);
	}
}
