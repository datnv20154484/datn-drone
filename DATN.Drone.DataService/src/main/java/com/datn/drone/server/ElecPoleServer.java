package com.datn.drone.server;

import java.util.List;

import javax.validation.Valid;

import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import com.datn.drone.controller.ElecPoleController;
import com.datn.drone.model.ElectricPole;


@RestController
@RequestMapping("/electricpoles")
public class ElecPoleServer {
	@Autowired
	private ElecPoleController elecpoleController;

	@RequestMapping(value = "/", method = RequestMethod.GET)
	public List<ElectricPole> getAllElecPoles() {
		return elecpoleController.getAllElecPoles();
	}

	@RequestMapping(value = "/getmaintenance", method = RequestMethod.GET)
	public List<ElectricPole> get() {
		return elecpoleController.getmaintenance();
	}
	
	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public ElectricPole getElecPoleById(@PathVariable("id") ObjectId id) {
		return elecpoleController.getElecPoleById(id);
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.PUT)
	public ResponseEntity<String> modifyElecPoleById(@PathVariable("id") ObjectId id, @Valid @RequestBody ElectricPole ElecPoles) {
		elecpoleController.modifyElecPoleById(id, ElecPoles);
		return new ResponseEntity<>("Sửa thông tin cột điện thành công", HttpStatus.OK);
	}
	
	@RequestMapping(value = "/", method = RequestMethod.POST)
	public ElectricPole createElecPole(@Valid @RequestBody ElectricPole ElecPoles) {
		return elecpoleController.createElecPole(ElecPoles);
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<String> deleteElecPole(@PathVariable ObjectId id) {
		elecpoleController.deleteElecPole(id);
		return new ResponseEntity<>("Xóa Cột điện thành công", HttpStatus.OK);
	}
}
