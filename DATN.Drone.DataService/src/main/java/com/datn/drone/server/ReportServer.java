package com.datn.drone.server;

import java.util.List;

import javax.validation.Valid;

import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.datn.drone.controller.ReportController;
import com.datn.drone.model.Report;

@RestController
@RequestMapping("/reports")
public class ReportServer {
	@Autowired
	private ReportController reportController;

	@RequestMapping(value = "/", method = RequestMethod.GET)
	public List<Report> getAllReports() {
		return reportController.getAllReports();
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public Report getReportById(@PathVariable("id") ObjectId id) {
		return reportController.getReportById(id);
	}@RequestMapping(value = "/{id}", method = RequestMethod.PUT)
	public ResponseEntity<String> modifyReportById(@PathVariable("id") ObjectId id,
			@Valid @RequestBody Report Reports) {
		reportController.modifyReportById(id, Reports);
		return new ResponseEntity<>("Sửa báo cáo thành công", HttpStatus.OK);
	
	}
	@RequestMapping(value = "/", method = RequestMethod.POST)
	public Report createReport(@Valid @RequestBody Report Reports) {
		return reportController.createReport(Reports);
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<String> delete(@PathVariable ObjectId id) {
		reportController.deleteReport(id);
		return new ResponseEntity<>("Xóa báo cáo thành công", HttpStatus.OK);
	}
}
