package com.datn.drone.server;

import java.util.List;
import javax.validation.Valid;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.datn.drone.controller.DroneController;
import com.datn.drone.model.Drone;

@RestController
@RequestMapping("/drones")
public class DroneServer {
	@Autowired
	private DroneController droneController;
	@RequestMapping(value = "/", method = RequestMethod.GET)
	public List<Drone> getAllDrones() {
		return droneController.getAllDrones();
	}
	@RequestMapping(value = "/status/{st}", method = RequestMethod.GET)
	public List<Drone> getAllMain(@PathVariable("st") String st) {
		return droneController.getdronestatus(st);
	}
	@RequestMapping(value = "/idman/{id}", method = RequestMethod.GET)
	public List<Drone> getalldronebyidman(@PathVariable("id") String id) {
		return droneController.getdronebyidman(id);
	}
	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public Drone getDrone(@PathVariable("id") ObjectId id) {
		return droneController.getDroneById(id);
	}

	@RequestMapping(value = "/getmaintenance", method = RequestMethod.GET)
	public List<Drone> get() {
		return droneController.getmaintenance();
	}
	@RequestMapping(value = "/updatebadmain", method = RequestMethod.PUT)
	public ResponseEntity<String> update() {
		droneController.updatemaintenance();
		return new ResponseEntity<>("Drone Status is upadate successsfully", HttpStatus.OK);
	}
	@RequestMapping(value = "/updategoodmain", method = RequestMethod.PUT)
	public ResponseEntity<String> updatedrone() {
		droneController.updatedronemaintenance();
		return new ResponseEntity<>("Drone Status is update successsfully", HttpStatus.OK);
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.PUT)
	public ResponseEntity<String> modifyDrone(@PathVariable("id") ObjectId id, @Valid @RequestBody Drone Drones) {
		droneController.modifyDroneById(id, Drones);
		return new ResponseEntity<>("Drone is modified successsfully", HttpStatus.OK);
	}

	@RequestMapping(value = "/", method = RequestMethod.POST)
	public Drone create(@Valid @RequestBody Drone Drones) {
		return droneController.createDrone(Drones);
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
	public ResponseEntity<String> delete(@PathVariable ObjectId id) {
		droneController.deleteDrone(id);
		return new ResponseEntity<>("Drone is deleted successsfully", HttpStatus.OK);
	}
}
