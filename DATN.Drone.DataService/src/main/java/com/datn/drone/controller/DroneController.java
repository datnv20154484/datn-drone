package com.datn.drone.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.datn.drone.model.Drone;
import com.datn.drone.repositories.DroneRepository;

@Service
public class DroneController {
	@Autowired
	private DroneRepository repository;
	long today = new Date().getTime();

	public List<Drone> getAllDrones() {
		return repository.findAll();
	}

	public Drone getDroneById(ObjectId id) {
		return repository.findBy_id(id);
	}

	public List<Drone> getmaintenance() {
		List<Drone> lt = repository.findAll();
		List<Drone> result = new ArrayList<Drone>();
		long maindate;
		for (Drone drone : lt) {
			maindate = drone.getMaintenancetime().getTime();
			if ((today - maindate > 0) || (today - maindate == 0)) {
				result.add(drone);
			}
		}
		return result;
	}

	public void updatemaintenance() {
		List<Drone> lt = repository.findAll();
		long maindate;
		for (Drone drone : lt) {
			maindate = drone.getMaintenancetime().getTime();
			if ((today - maindate > 0) || (today - maindate == 0)) {
				Drone d1 = new Drone();
				d1.setName(drone.getName());
				d1.setDes(drone.getDes());
				d1.setIdman(drone.getIdman());
				d1.setMaintenancetime(drone.getMaintenancetime());
				d1.setStatus("bad");
				modifyDroneById(drone._id, d1);
			}
		}
	}

	public void updatedronemaintenance() {
		List<Drone> lt = repository.findAll();
		long maindate;
		for (Drone drone : lt) {
			maindate = drone.getMaintenancetime().getTime();
			if ((today - maindate < 0) && (drone.getStatus().equals("bad"))) {
				Drone d1 = new Drone();
				d1.setName(drone.getName());
				d1.setDes(drone.getDes());
				d1.setIdman(drone.getIdman());
				d1.setMaintenancetime(drone.getMaintenancetime());
				d1.setStatus("good");
				modifyDroneById(drone._id, d1);
			}
		}
	}

	public void modifyDroneById(ObjectId id,Drone Drones) {
		Drones.set_id(id);
		repository.save(Drones);
	}

	public Drone createDrone(Drone Drones) {
		Drones.set_id(ObjectId.get());
		repository.save(Drones);
		return Drones;
	}

	public void deleteDrone(ObjectId id) {
		repository.delete(repository.findBy_id(id));
	}
	public List<Drone> getdronestatus(String st){
		return repository.findBystatus(st);
	}
	public List<Drone> getdronebyidman(String id){
		return repository.findByidman(id);
	}
}
