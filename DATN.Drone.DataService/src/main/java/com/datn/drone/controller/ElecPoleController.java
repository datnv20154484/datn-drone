package com.datn.drone.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.datn.drone.model.ElectricPole;
import com.datn.drone.repositories.ElecPoleRepository;

@Service
public class ElecPoleController {
	@Autowired
	private ElecPoleRepository repository;
	long today = new Date().getTime();

	public List<ElectricPole> getAllElecPoles() {
		return repository.findAll();
	}

	public ElectricPole getElecPoleById(ObjectId id) {
		return repository.findBy_id(id);
	}

	public void modifyElecPoleById(ObjectId id,ElectricPole ElecPoles) {
		ElecPoles.set_id(id);
		repository.save(ElecPoles);
	}

	public ElectricPole createElecPole(ElectricPole ElecPoles) {
		ElecPoles.set_id(ObjectId.get());
		repository.save(ElecPoles);
		return ElecPoles;
	}

	public void deleteElecPole(ObjectId id) {
		repository.delete(repository.findBy_id(id));
	}
	public List<ElectricPole> getmaintenance() {
		List<ElectricPole> lt = repository.findAll();
		List<ElectricPole> result = new ArrayList<ElectricPole>();
		long maindate;
		for (ElectricPole elecpole : lt) {
			maindate = elecpole.getEP_MaintenanceTime().getTime();
			if ((today - maindate > 0) || (today - maindate == 0)) {
				result.add(elecpole);
			}
		}
		return result;
	}

}
