package com.datn.drone.controller;

import java.util.List;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;
import com.datn.drone.controller.AES;
import com.datn.drone.model.Employee;
import com.datn.drone.repositories.EmployeeRepository;

@Service
public class EmployeeController {
	@Autowired
	private EmployeeRepository repository;

	@Autowired
	private JavaMailSender javaMailSender;

	private String secretKey = "@datdeptrai@@@anhxinhdep@@@910132!!!";

	public List<Employee> getAllemployees() {
		return repository.findAll();
	}

	public Employee getEmpById(ObjectId id) {
		return repository.findBy_id(id);
	}

	public Employee getEmpByusername(String username) {
		return repository.findByusername(username);
	}

	public Employee getEmpBynationid(String nationid) {
		return repository.findBynationid(nationid);
	}

	public Employee getEmpBymail(String mail) {
		return repository.findBymail(mail);
	}

	public Employee getEmpByphone(String phone) {
		return repository.findByphone(phone);
	}

	public List<Employee> getEmpByrole(String role) {
		return repository.findByrole(role);
	}

	public List<Employee> getEmpBybirth(String birth) {
		return repository.findBybirth(birth);
	}

	public List<Employee> getEmpByname(String name) {
		return repository.findByname(name);
	}

	public List<Employee> getEmpBysex(String sex) {
		return repository.findBysex(sex);
	}

	public List<Employee> getEmpByaddress(String add) {
		return repository.findByaddress(add);
	}

	public void modifyEmpById(ObjectId id, Employee employees) {
		employees.set_id(id);
		repository.save(employees);
	}

	public boolean createEmp(Employee employees) {
		employees.set_id(ObjectId.get());
		int check = 0;
		boolean result = false;
		List<Employee> checklist = repository.findAll();
		for (Employee emp : checklist) {
			if (emp.getUsername().equals(employees.getUsername()) || emp.getNationid().equals(employees.getNationid())
					|| emp.mail.equals(employees.getMail()) || emp.getPhone().equals(employees.getPhone())) {
				check++;
			}
		}
		if (check == 0) {
			String pass = AES.encrypt(employees.getPassword(), secretKey);
			employees.setPassword(pass);
			repository.save(employees);
			result = true;
		}
		return result;
	}

	public void deleteEmp(ObjectId id) {
		repository.delete(repository.findBy_id(id));
	}

	public Employee login(String username, String password) {
		Employee emp = repository.findByusername(username);
		String pass = AES.decrypt(emp.getPassword(), secretKey);
		String passconfig = AES.decrypt(password, secretKey);
		Employee result = null;
		if (pass.equals(passconfig)) {
			result = emp;
		}
		return result;
	}

	public void sendforgetmail(String phone) {
		Employee emp = repository.findByphone(phone);
		SimpleMailMessage msg = new SimpleMailMessage();
		msg.setTo(emp.getMail());
		msg.setSubject("Lấy lại thông tin người dùng");
		msg.setText("Họ và tên: " + emp.getName() + "\nSố điện thoại: " + emp.getPhone() + "\nTài khoản: "
				+ emp.getUsername() + "\nMật khẩu: " + AES.decrypt(emp.getPassword(), secretKey));

		javaMailSender.send(msg);
	}
}