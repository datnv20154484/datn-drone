package com.datn.drone.controller;

import java.util.List;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.datn.drone.model.Report;
import com.datn.drone.repositories.ReportRepository;
@Service
public class ReportController {
	@Autowired
	private ReportRepository repository;

	public List<Report> getAllReports() {
		return repository.findAll();
	}

	public Report getReportById(ObjectId id) {
		return repository.findBy_id(id);
	}

	public void modifyReportById(ObjectId id, Report Reports) {
		Reports.set_id(id);
		repository.save(Reports);
	}

	public Report createReport(Report Reports) {
		Reports.set_id(ObjectId.get());
		repository.save(Reports);
		return Reports;
	}

	public void deleteReport(ObjectId id) {
		repository.delete(repository.findBy_id(id));
	}
}
