package com.datn.drone.repositories;

import java.util.List;

import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;
import com.datn.drone.model.Drone;

public interface DroneRepository extends MongoRepository<Drone, String> {
	Drone findBy_id(ObjectId _id);
	List<Drone> findBystatus(String status);
	List<Drone> findByidman(String id);
}