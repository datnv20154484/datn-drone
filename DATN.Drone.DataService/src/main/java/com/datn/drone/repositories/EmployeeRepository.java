package com.datn.drone.repositories;

import java.util.List;

import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;

import com.datn.drone.model.Employee;

public interface EmployeeRepository extends MongoRepository<Employee, String> {
	Employee findBy_id(ObjectId _id);
	Employee findByusername(String username);
	List<Employee> findByrole(String role);
	List<Employee> findByname(String name);
	List<Employee> findBybirth(String birth);
	List<Employee> findBysex(String sex);
	Employee findBynationid(String nationid);
	List<Employee> findByaddress(String address);
	Employee findBymail(String mail);
	Employee findByphone(String phone);
}
