package com.datn.drone.repositories;

import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;
import com.datn.drone.model.ElectricPole;

public interface ElecPoleRepository extends MongoRepository<ElectricPole, String> {
	ElectricPole findBy_id(ObjectId _id);
}