package com.datn.drone.repositories;

import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;
import com.datn.drone.model.Report;


public interface ReportRepository extends MongoRepository<Report, String> {
	Report findBy_id(ObjectId _id);
}