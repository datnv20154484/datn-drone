package com.datn.drone.repository;

import java.util.List;

import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;
import com.datn.drone.model.Incident;

public interface IncidentRepository extends MongoRepository<Incident, String> {
	Incident findBy_id(ObjectId _id);
	//Incident findByidpole(String idpole);
	List<Incident> findBystatus(String status);
	List<Incident> findBylevel(String level);
	List<Incident> findByidpole(String idpole);

}
