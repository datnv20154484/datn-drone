package com.datn.drone.model;

import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "Incident")
public class Incident {
	@Id
	public ObjectId _id;
	public String name;
	public String des;
	public String date;
	public String status;
	public String level;
	public String idfix;
	public String iddetect;
	public String idpole;
	public String image;
	
	public String get_id() {
		return _id.toHexString();
	}

	public void set_id(ObjectId _id) {
		this._id = _id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDes() {
		return des;
	}

	public void setDes(String des) {
		this.des = des;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getLevel() {
		return level;
	}

	public void setLevel(String level) {
		this.level = level;
	}

	public String getIdfix() {
		return idfix;
	}

	public void setIdfix(String idfix) {
		this.idfix = idfix;
	}

	public String getIdpole() {
		return idpole;
	}

	public void setIdpole(String idpole) {
		this.idpole = idpole;
	}

	public String getIddetect() {
		return iddetect;
	}

	public void setIddetect(String iddetect) {
		this.iddetect = iddetect;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}
	
}
