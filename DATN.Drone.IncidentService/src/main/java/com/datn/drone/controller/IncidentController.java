package com.datn.drone.controller;

import java.util.List;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.datn.drone.model.Incident;
import com.datn.drone.repository.IncidentRepository;

@Service
public class IncidentController {

	@Autowired
	private IncidentRepository IncidentRepo;

	public List<Incident> getAllIncident() {
		return IncidentRepo.findAll();
	}

	public Incident getIncident(String id) {
		return IncidentRepo.findById(id).get();
	}

	public void addIncident(Incident incident){
		incident.set_id(ObjectId.get());
		IncidentRepo.save(incident);
	}

	public void deleteIncident(String id) {
		IncidentRepo.deleteById(id);
	}

	public void modifyIncident(ObjectId id, Incident i) {
		i.set_id(id);
		IncidentRepo.save(i);
	}

	public List<Incident> getincistatus(String status) {
		return IncidentRepo.findBystatus(status);
	}
	
	public List<Incident> getincilevel(String level){
		return IncidentRepo.findBylevel(level);
	}
	
	public List<Incident> getincipole(String idpole){
		return IncidentRepo.findByidpole(idpole);
	}
}
